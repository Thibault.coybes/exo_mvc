<?php

$routes = array(
    array('home', 'default', 'index'),
    array('dashboard', 'user', 'dashboard'),
    array('listingAbonnes', 'user', 'listingAbonnes'),
    array('deleteUser', 'user', 'deleteUser', array('id')),
    array('addUser', 'user', 'addUser'),
    array('editUser', 'user', 'editUser', array('id')),
    array('addProduct', 'product', 'addProduct'),
    array('listingProduct', 'product', 'listingProduct'),
    array('editProduct', 'product', 'editProduct', array('id')),
    array('deleteProduct', 'product', 'deleteProduct', array('id')),
    array('addBorrow', 'borrow', 'addBorrow'),
    array('listingBorrow', 'borrow', 'listingBorrow'),
    array('endBorrow', 'borrow', 'endBorrow', array('id')),
);
