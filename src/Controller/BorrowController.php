<?php

namespace App\Controller;

use App\Model\BorrowModel;
use App\Service\Form;
use Core\Kernel\AbstractController;

class BorrowController extends AbstractController
{
    public function addBorrow()
    {
        $title = 'Ajouter un emprunt';
        $allUser = BorrowModel::getAllUser();
        $allProduct = BorrowModel::getAllProduct();
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            if (!empty($post['Abonné'] && $post['Produit'])) {
                BorrowModel::insert($post);
                $this->redirect('listingBorrow');
            }
        }
        $form = new Form($error);
        $this->render('app.admin.borrow.addBorrow', array(
            'form' => $form,
            'title' => $title,
            'allUser' => $allUser,
            'allProduct' => $allProduct,
        ), 'admin');
    }
    public function listingBorrow()
    {
        $onGoingBorrows = BorrowModel::getOnGoingBorrow();
        $endBorrows = BorrowModel::getEndBorrow();
        $this->render('app.admin.borrow.listingBorrow', array(
            'onGoingBorrows' => $onGoingBorrows,
            'endBorrows' => $endBorrows,
        ), 'admin');
    }
    public function endBorrow($id)
    {
        if (empty($id) || is_null($id) || empty(BorrowModel::findById($id))) {
            $this->Abort404();
        } else {
            BorrowModel::update($id);
            $this->redirect('listingBorrow');
        }
    }
}
