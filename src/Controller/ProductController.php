<?php

namespace App\Controller;

use App\Model\ProductModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class ProductController extends AbstractController
{
    public function addProduct()
    {
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $error['titre'] = $v->textValid($post['titre'], 'titre', 2, 100);
            $error['reference'] = $v->textValid($post['reference'], 'reference', 2, 100);
            $error['description'] = $v->textValid($post['description'], 'description', 10, 255);
            if ($v->IsValid($error)) {
                ProductModel::insert($post);
                $this->redirect('listingProduct');
            }
        }
        $form = new Form($error);
        $this->render('app.admin.product.addProduct', array(
            'form' => $form,
        ), 'admin');
    }
    public function listingProduct()
    {
        $titre = 'Listing Produits';
        $products = ProductModel::getAllproduct();
        $this->render('app.admin.product.listingProduct', array(
            'titre' => $titre,
            'products' => $products,
        ), 'admin');
    }
    public function editProduct($id)
    {
        if (empty($id) || is_null($id) || empty(ProductModel::findById($id))) {
            $this->redirect('404');
        } else {
            $product = ProductModel::findById($id);
            $error = [];
            if (!empty($_POST['submitted'])) {
                $post = $this->cleanXss($_POST);
                $v = new Validation();
                $error['titre'] = $v->textValid($post['titre'], 'titre', 2, 100);
                $error['reference'] = $v->textValid($post['reference'], 'reference', 2, 100);
                $error['description'] = $v->textValid($post['description'], 'description', 10, 255);
                if ($v->IsValid($error)) {
                    ProductModel::update($id, $post);
                    $this->redirect('listingProduct');
                }
            }
            $form = new Form($error);
        }
        $this->render('app.admin.product.editProduct', array(
            'form' => $form,
            'product' => $product,
        ), 'admin');
    }
    public function deleteProduct($id)
    {
        if (empty($id) || is_null($id) || empty(ProductModel::findById($id))) {
            $this->redirect('404');
        } else {
            ProductModel::delete($id);
            $this->redirect('listingProduct');
        }
    }
}
