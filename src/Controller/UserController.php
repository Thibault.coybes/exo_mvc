<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class UserController extends AbstractController
{
    public function dashboard()
    {

        $this->render('app.admin.dashboard', array(), 'admin');
    }
    public function listingAbonnes()
    {
        $title = 'Listing Abonnés';
        $allAbonnes = UserModel::getAllAbonnes();
        $this->render('app.admin.user.listingAbonnes', array(
            'title' => $title,
            'users' => $allAbonnes,
        ), 'admin');
    }
    public function deleteUser($id)
    {
        if (empty($id) || is_null($id) || empty(UserModel::findById($id))) {
            $this->redirect('404');
        } else {
            UserModel::delete($id);
            $this->redirect('listingAbonnes');
        }
    }
    public function addUser()
    {
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $error['nom'] = $v->textValid($post['nom'], 'nom', 2, 100);
            $error['prenom'] = $v->textValid($post['prenom'], 'prenom', 2, 100);
            $error['email'] = $v->emailValid($post['email']);
            if (UserModel::findIdByEmail($post['email'])) {
                $error['email'] = 'Cette adresse email et déja utilisé.';
            }
            $error['age'] = '';
            if (empty($post['age']) || is_int($post['age'])) {
                $error['age'] = 'Veuillez renseigner un age valide';
            } elseif ($post['age'] < 18) {
                $error['age'] = 'Vous êtes trop jeunes.';
            }
            if ($v->IsValid($error)) {
                UserModel::insert($post);
                $this->redirect('listingAbonnes');
            }
        }


        $form = new Form($error);
        $this->render('app.admin.user.addUser', array(
            'form' => $form,
        ), 'admin');
    }

    public function editUser($id)
    {
        $error = [];
        $user = UserModel::findById($id);
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $error['nom'] = $v->textValid($post['nom'], 'nom', 2, 100);
            $error['prenom'] = $v->textValid($post['prenom'], 'prenom', 2, 100);
            $error['email'] = $v->emailValid($post['email']);
            if (UserModel::findIdByEmail($post['email'])) {
                $error['email'] = 'Cette adresse email et déja utilisé.';
            }
            $error['age'] = '';
            if (empty($post['age']) || is_int($post['age'])) {
                $error['age'] = 'Veuillez renseigner un age valide';
            } elseif ($post['age'] < 18) {
                $error['age'] = 'Vous êtes trop jeunes.';
            }
            if ($v->IsValid($error)) {
                UserModel::update($id, $post);
                $this->redirect('listingAbonnes');
            }
        }
        $form = new Form($error);
        $this->render('app.admin.user.editUser', array(
            'form' => $form,
            'user' => $user,
        ), 'admin');
    }
}
