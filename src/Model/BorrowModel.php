<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class BorrowModel extends AbstractModel
{
    public static function getAllUser()
    {
        return App::getDatabase()->query("SELECT * FROM abonnes", get_called_class());
    }
    public static function getAllProduct()
    {
        return App::getDatabase()->query("SELECT * FROM products", get_called_class());
    }
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO borrows (id_abonne, id_product, date_start) VALUES (?,?,NOW())",
            array($post['Abonné'], $post['Produit'])
        );
    }
    public static function update($id)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE borrows SET date_end = NOW() WHERE id = ?",
            array($id)
        );
    }
    public static function getOnGoingBorrow()
    {
        return App::getDatabase()->query("SELECT * FROM borrows WHERE date_end is NULL", get_called_class());
    }
    public static function getEndBorrow()
    {
        return App::getDatabase()->query("SELECT * FROM borrows WHERE date_end is not NULL", get_called_class());
    }
    public static function getUserByBorrowTable($id)
    {
        return App::getDatabase()->query("SELECT nom, prenom
        FROM borrows
        LEFT JOIN abonnes ON $id = abonnes.id", get_called_class());
    }
    public static function getProductByBorrowTable($id)
    {
        return App::getDatabase()->query("SELECT titre
        FROM borrows
        LEFT JOIN products ON $id = products.id", get_called_class());
    }
    public static function findById($id, $columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM borrows WHERE " . $columId . " = ?", [$id], get_called_class());
    }
}
