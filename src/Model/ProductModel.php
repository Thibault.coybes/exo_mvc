<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductModel extends AbstractModel
{
    protected static $table = 'products';
    public static function getAllproduct()
    {
        return App::getDatabase()->query("SELECT * FROM " . self::$table, get_called_class());
    }
    public static function delete($id, $columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::$table . " WHERE " . $columId . " = ?", [$id], get_called_class(), true);
    }
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (titre, reference, description) VALUES (?,?,?)",
            array($post['titre'], $post['reference'], $post['description'])
        );
    }
    public static function findIdByReference($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::$table . " WHERE email = ?", [$email], get_called_class(), true);
    }
    public static function findById($id, $columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::$table . " WHERE " . $columId . " = ?", [$id], get_called_class(), true);
    }
    public static function update($id, $post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ? WHERE id = ?",
            array($post['titre'], $post['reference'], $post['description'], $id)
        );
    }
}
