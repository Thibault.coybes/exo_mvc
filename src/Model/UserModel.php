<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected static $table = 'abonnes';
    public static function getAllAbonnes()
    {
        return App::getDatabase()->query("SELECT * FROM " . self::$table, get_called_class());
    }
    public static function delete($id, $columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::$table . " WHERE " . $columId . " = ?", [$id], get_called_class(), true);
    }
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, prenom, age, email, created_at) VALUES (?,?,?,?,NOW())",
            array($post['nom'], $post['prenom'], $post['age'], $post['email'])
        );
    }
    public static function findIdByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::$table . " WHERE email = ?", [$email], get_called_class(), true);
    }
    public static function findById($id, $columId = 'id')
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::$table . " WHERE " . $columId . " = ?", [$id], get_called_class(), true);
    }
    public static function update($id, $post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET nom = ?, prenom = ?, age = ?, email = ?, modified_at = NOW() WHERE id = ?",
            array($post['nom'], $post['prenom'], $post['age'], $post['email'], $id)
        );
    }
}
