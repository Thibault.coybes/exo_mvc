<div class="containerBorrow">
    <h1>Ajouter un nouveau produit</h1>

    <form action="" method="post">
        <?= $form->label('Abonné') ?>
        <?= $form->selectEntity('Abonné', $allUser, 'nom') ?>
        <?= $form->error('Abonné') ?>

        <?= $form->label('Produit') ?>
        <?= $form->selectEntity('Produit', $allProduct, 'titre') ?>
        <?= $form->error('Produit') ?>

        <?= $form->submit('submitted') ?>

    </form>
</div>