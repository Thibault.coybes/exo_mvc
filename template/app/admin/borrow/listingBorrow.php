<div class="tableau">
    <h2>Emprunt en cour</h2>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Abonné</th>
                <th>Produit</th>
                <th>Début d'emprunt</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
            <?php

            use App\Model\BorrowModel;

            foreach ($onGoingBorrows as $onGoingBorrow) {
                echo ' <tr>
                    <td>' . $onGoingBorrow->id . '</td>
                    <td>' . BorrowModel::getUserByBorrowTable($onGoingBorrow->id_abonne)[0]->nom . '</td>
                    <td>' . BorrowModel::getProductByBorrowTable($onGoingBorrow->id_product)[0]->titre . '</td>
                    <td>' . $onGoingBorrow->date_start . '</td>
                    <td class="options" ><a href="' . $view->path('endBorrow', array('id' => $onGoingBorrow->id)) . '" class="end">Mettre Fin</a></td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>

<div class="tableau">
    <h2>Historique des emprunts</h2>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Abonné</th>
                <th>Produit</th>
                <th>Début d'emprunt</th>
                <th>Fin d'emprunt</th>
            </tr>
        </thead>
        <tbody>
            <?php

            foreach ($endBorrows as $endBorrow) {
                echo ' <tr>
                    <td>' . $endBorrow->id . '</td>
                    <td>' . BorrowModel::getUserByBorrowTable($endBorrow->id_abonne)[0]->nom . '</td>
                    <td>' . BorrowModel::getProductByBorrowTable($endBorrow->id_product)[0]->titre . '</td>
                    <td>' . $endBorrow->date_start . '</td>
                    <td>' . $endBorrow->date_end . '</td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>