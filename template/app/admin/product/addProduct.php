<div class="containerProduct">
    <h1>Ajouter un nouveau produit</h1>

    <form action="" method="post">
        <?= $form->label('titre') ?>
        <?= $form->input('titre') ?>
        <?= $form->error('titre') ?>

        <?= $form->label('reference') ?>
        <?= $form->input('reference') ?>
        <?= $form->error('reference') ?>

        <?= $form->label('description') ?>
        <?= $form->textarea('description') ?>
        <?= $form->error('description') ?>


        <?= $form->submit('submitted') ?>

    </form>
</div>