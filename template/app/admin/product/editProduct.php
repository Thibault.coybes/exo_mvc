<div class="containerProduct">
    <h1>Modifier le produit</h1>

    <form action="" method="post">
        <?= $form->label('titre') ?>
        <?= $form->input('titre', 'text', $product->titre) ?>
        <?= $form->error('titre') ?>

        <?= $form->label('reference') ?>
        <?= $form->input('reference', 'text', $product->reference) ?>
        <?= $form->error('reference') ?>

        <?= $form->label('description') ?>
        <?= $form->textarea('description', $product->description) ?>
        <?= $form->error('description') ?>


        <?= $form->submit('submitted') ?>

    </form>
</div>