<div class="tableau">
    <h2><?= $titre ?></h2>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Titre</th>
                <th>Référence</th>
                <th>Description</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($products as $product) {
                echo ' <tr>
                    <td>' . $product->id . '</td>
                    <td>' . $product->titre . '</td>
                    <td>' . $product->reference . '</td>
                    <td>' . $product->description . '</td>
                    <td class="options"> <a class="delete" href=' . $view->path("deleteProduct", array('id' => $product->id)) . '>Delete</a> <a class="edit" href="' . $view->path('editProduct', array('id' => $product->id)) . '">Edit</a> </td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>