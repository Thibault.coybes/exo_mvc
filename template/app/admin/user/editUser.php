<div class="containerEdit">
    <h1>Modifier le profil</h1>

    <form action="" method="post">
        <?= $form->label('nom') ?>
        <?= $form->input('nom', 'text', $user->nom) ?>
        <?= $form->error('nom') ?>

        <?= $form->label('prenom') ?>
        <?= $form->input('prenom', 'text', $user->prenom) ?>
        <?= $form->error('prenom') ?>

        <?= $form->label('email') ?>
        <?= $form->input('email', 'text', $user->email) ?>
        <?= $form->error('email') ?>

        <?= $form->label('age') ?>
        <?= $form->input('age', 'text', $user->age) ?>
        <?= $form->error('age') ?>

        <?= $form->submit('submitted') ?>

    </form>
</div>