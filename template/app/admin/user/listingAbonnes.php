<div class="tableau">
    <h2><?= $title ?></h2>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Age</th>
                <th>Email</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($users as $user) {
                echo ' <tr>
                    <td>' . $user->id . '</td>
                    <td>' . $user->nom . '</td>
                    <td>' . $user->prenom . '</td>
                    <td>' . $user->age . '</td>
                    <td>' . $user->email  . '</td>
                    <td class="options"> <a class="delete" href=' . $view->path("deleteUser", array('id' => $user->id)) . '>Delete</a> <a class="edit" href="' . $view->path('editUser', array('id' => $user->id)) . '">Edit</a> </td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>