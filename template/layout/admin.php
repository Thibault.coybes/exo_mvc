<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>

<body>
    <div class="content">
        <div class="sidebar">
            <div class="container">
                <h1>Admin</h1>
                <div class="category">
                    <ul>
                        <li>
                            <h2><a href="<?= $view->path('dashboard') ?>"><i class="fa-solid fa-chart-line"></i>Dashboard</a></h2>
                        </li>
                        <li>
                            <h2><i class="fa-solid fa-user"></i>Abonnés</h2>
                            <a href="<?= $view->path('listingAbonnes') ?>">- Listing Abonnés</a>
                            <a href="<?= $view->path('addUser') ?>">- Ajouter Abonné</a>
                        </li>
                        <li>
                            <h2><i class="fa-solid fa-cart-shopping"></i>Produits</h2>
                            <a href="<?= $view->path('listingProduct') ?>">- Listing Produits</a>
                            <a href="<?= $view->path('addProduct') ?>">- Ajouter Produit</a>
                        </li>
                        <li>
                            <h2><i class="fa-solid fa-arrow-rotate-left"></i>Emprunts</h2>
                            <a href="<?= $view->path('listingBorrow') ?>">- Listing Emprunts</a>
                            <a href="<?= $view->path('addBorrow') ?>">- Ajouter Emprunt</a>
                        </li>
                    </ul>
                </div>
                <div class="return">
                    <a href="<?= $view->path('home') ?>"><i class="fa-solid fa-arrow-left"></i></a>
                </div>
            </div>
        </div>
        <div class="container">
            <?= $content; ?>
        </div>
    </div>




    <footer id="colophon">
        <div class="wrap">
            <p>MVC 6 - Framework Pédagogique.</p>
        </div>
    </footer>
    <?php echo $view->add_webpack_script('admin'); ?>
</body>

</html>